﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="WebApplication2.Contact" MaintainScrollPositionOnPostback="true"  %>
    
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Group controls</h3>
   
           
    <div style="white-space: nowrap">
        <div class="wrapper">
            <asp:Label runat="server" ID="LabelOne" Text="SubmitOne:" SkinID="1"  />
            <asp:TextBox runat="server" ID="TextBox1" SkinID="1"  />
            <asp:Button runat="server" ID="SubmitOne" OnClick="SubmitOne_Click"  SkinID="1"  />
        </div>
         <div class="wrapper">
         <asp:Label runat="server" ID="LabelTwo" Text="SubmitTwo:" SkinID="1" />                    
        <asp:TextBox runat="server" ID="TextBoxTwo" SkinID="1"  />                      
        <asp:Button runat="server" ID="SubmitTwo" OnClick="SubmitTwo_Click" SkinID="1"  />
        </div>
    </div>


</asp:Content>
